<?php

namespace Drupal\weplant\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Drupal\migrate\Row;


/**
 * Source plugin for press_node content.
 *
 * @MigrateSource(
 *   id = "press_node"
 * )
 */
class PressNode extends SqlBase {

    /**
     * {@inheritdoc}
     */
    public function query() {

        $query = $this->select('node_revision')
            ->fields('n', array(
                'nid',
                'type',
                'tnid',
                'translate',
            ))
            ->fields('node_revision', array(
                'vid',
                'title',
                'log',
                'timestamp',
            ))
            ->fields('field_revision_body', array(
                'revision_id',
                'body_value',
            ))
            ->fields('field_data_field_featured', array(
                'revision_id',
                'field_featured_value',
            ))
            ->fields('field_data_field_press_date', array(
                'revision_id',
                'field_press_date_value',
            ))
            ->fields('field_data_field_press_type', array(
                'revision_id',
                'field_press_type_tid',
            ));


        $query->condition('n.type', 'press');

        $query->innerJoin('node', 'n', 'n.vid = node_revision.vid');
        $query->innerJoin('field_revision_body', 'field_revision_body', 'n.vid = field_revision_body.revision_id');
        $query->innerJoin('field_data_field_featured', 'field_data_field_featured', 'n.vid = field_data_field_featured.revision_id');
        $query->innerJoin('field_data_field_press_date', 'field_data_field_press_date', 'n.vid = field_data_field_press_date.revision_id');
        $query->innerJoin('field_data_field_press_type', 'field_data_field_press_type', 'n.vid = field_data_field_press_type.revision_id');



        return $query;
    }

    /**
     * {@inheritdoc}
     */
    public function fields() {
        $fields = array(
            'nid' => $this->t('Node ID'),
            'type' => $this->t('Type'),
            'title' => $this->t('Title'),
            'body' => $this->t('Body'),
        );
        return $fields;
    }

    /**
     * {@inheritdoc}
     */
    public function getIds() {
        $ids['nid']['type'] = 'integer';
        $ids['nid']['alias'] = 'n';
        return $ids;
    }

     /**
      * {@inheritdoc}
      */
    public function prepareRow(Row $row) {
        $timestamp = strtotime($row->getSourceProperty('field_press_date_value'));
        $timestamp = date("Y-m-d", $timestamp);
        $row->setSourceProperty('field_press_date_value', $timestamp);

        $press_type = $row->getSourceProperty('field_press_type_tid');
        if($press_type == '29'){
            $press_type = '1';
        }else{
            $press_type = '2';
        }
        $row->setSourceProperty('field_press_type_tid', $press_type);

        $row->setSourceProperty('node_uid', 1);


        return parent::prepareRow($row);
    }

}
